import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Toevoegen van ingredienten',
      description: 'Hiermee kan een administrator een ingredient toevoegen',
      scenario: [
        'De actor gaat naar het knopje ingredienten.',
        'De actor drukt op "toevoegen van ingredient"',
        'De actor vult de gegevens van het nieuwe ingredient toe',
        'De actor drukt op de knop opslaan.',
        'De applicatie valideert de velden',
        'Als alles correct is ingevoerd zal de applicatie deze opslaan en een melding laten zien dat dit succesvol is opgeslagen'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd en heeft een beheer account',
      postcondition: 'Er is een ingredient toegevoegd.'
    },
    {
      id: 'UC-',
      name: 'Verwijderen van ingredienten',
      description: 'Een administrator kan ingredienten verwijderen',
      scenario: [
        'De actor gaat naar het knopje ingredienten.',
        'Het systeem toont de opgeslagen ingredienten naar de gebruiker',
        'De actor drukt op het verwijder icoon bij de geselecteerde ingredient',
        'Het systeem verwijderd de geselecteerde ingredient en toont een succes melding'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd en heeft een beheer account',
      postcondition: 'Er is een ingredient verwijderd.'
    },
    {
      id: 'UC-',
      name: 'Ophalen van ingredienten',
      description: 'Een gebruiker kan zien welke ingredienten er beschikbaar zijn.',
      scenario: [
        'De actor gaat naar het knopje ingredienten.',
        'Het systeem toont de opgeslagen ingredienten naar de gebruiker'
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd en heeft een beheer account',
      postcondition: 'Er is een lijst met ingredienten naar de actor gestuurd.'
    },
    {
      id: 'UC-',
      name: 'Maken van een burger',
      description: 'Hiermee kan een normale gebruiker van de applicatie zijn eigen burger maken',
      scenario: [
        'De actor druk op het knopje "toevoegen van een burger."',
        'De actor kiest uit de ingredienten die hij/zij op zijn burger wilt.',
        'De actor vult de naam in van de burger.',
        'De actor drukt op de knop opslaan.',
        'De applicatie valideert de velden',
        'Als alles correct is ingevoerd zal de applicatie deze opslaan.',
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De burger is succesvol opgeslagen onder de gebruiker zijn account.'
    },
    {
      id: 'UC-',
      name: 'Verwijderen van een gemaakte burger',
      description: 'Hiermee kan een normale gebruiker van de applicatie zijn eigen gemaakte burger verwijderen van de site',
      scenario: [
        'De actor druk op zijn account',
        'Het systeem geeft de gebruiker zijn account en gemaakte burgers terug.',
        'De actor drukt op het verwijder icoon van de burger die hij wilt verwijderen',
        'Het systeem verwijderd de burger van de gebruiker en toont een succes melding terug naar de gebruiker'

      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De burger is succesvol verwijderd.'
    },
    {
      id: 'UC-',
      name: 'Aanpassen van een gemaakte burger',
      description: 'Hiermee kan een normale gebruiker van de applicatie zijn eigen gemaakte burger verwijderen van de site',
      scenario: [
        'De actor druk op zijn account',
        'Het systeem geeft de gebruiker zijn account en gemaakte burgers terug.',
        'De actor drukt op het update icoon van de burger die hij wilt verwijderen',
        'Het systeem geeft de gebruiker ingevulden velden aan de actor met de gegevens van de geselecteerde burger',
        'De actor voert de aangepaste data door en drukt op opslaan',
        'Het systeem slaat de aangepaste data op en toont een succes melding terug naar de gebruiker'

      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De burger is succesvol aangepast.'
    },
    {
      id: 'UC-',
      name: 'Het bekijken van gemaakte burgers',
      description: 'Hiermee kan een normale gebruiker van de applicatie zijn eigen burgers zien',
      scenario: [
        'De actor drukt op het knopje "gemaakte burgers" ',
        'Het systeem toont de gemaakte burgers gemaakt door andere.'
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor krijgt een lijst met gemaakte burgers te zien.'
    },
    {
      id: 'UC-',
      name: 'Burger opslaan als favoriet',
      description: 'Hiermee kan een normale gebruiker van de applicatie een gemaakte burger opslaan als favoriet',
      scenario: [
        'De actor drukt op het knopje "gemaakte burgers" ',
        'De actor krijgt een lijst te zien met gemaakte burgers',
        'Het systeem toont de gemaakte burgers gemaakt door andere.',
        'De actor klikt een burger aan.',
        'De actor klikt vervolgens op "plaats in favorieten" knop',
        'Het systeem slaat de gebruiker zijn keuze op en toont een succes melding'
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor heeft succesvol een burger toegevoegd aan zijn favorieten.'
    },
    {
      id: 'UC-',
      name: 'Burger verwijderen als favoriet',
      description: 'Hiermee kan een normale gebruiker burger die hij als favoriet gemarkeerd heeft verwijderen.',
      scenario: [
        'De actor drukt op zijn account',
        'Het systeem toont de gebruiker zijn account gegevens samen met zijn gemaakte burgers',
        'De actor drukt op de knop "verwijderen uit favorieten"',
        'Het systeem verwijderd de opgeslagen burger en toont een succes melding naar de gebruiker'
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor heeft succesvol zijn favoriete burger verwijderd.'
    },
    {
      id: 'UC-',
      name: 'Gebruiker kan zijn gemaakte burgers zien',
      description: 'Hiermee kan een normale gebruiker zijn gemaakte burgers zien.',
      scenario: [
        'De actor drukt op zijn account',
        'Het systeem toont de gebruiker zijn account gegevens samen met zijn gemaakte burgers'
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor heeft succesvol zijn gemaakte burgers terug gekregen in de pagina.'
    },
    {
      id: 'UC-',
      name: 'Gebruiker kan zijn favoriete burgers zien',
      description: 'Hiermee kan een normale gebruiker zijn gemaakte burgers zien.',
      scenario: [
        'De actor drukt op zijn account',
        'Het systeem toont de gebruiker zijn account gegevens samen met zijn gemaakte burgers'
      ],
      actor: 'Reguliere ingelogde gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor heeft succesvol zijn gemaakte burgers terug gekregen in de pagina.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
